#ifndef MAZEUTILS_H_URRNJKT0
#define MAZEUTILS_H_URRNJKT0

#include <cstdlib>
#include <string>

// number of walls/directions and enum
const uint WALLS = 4;
enum Walls {EAST, SOUTH, WEST, NORTH};

class Maze {
    int **adjacents_; // adjacency list
    uint *set_map_, *set_size_; // set data
    uint num_rows_, num_columns_, size_;
    uint set_count_;
    const bool Border(const int cell, const uint wall) const;
    const int Neighbor(const int cell, const uint wall) const;
    const uint Wall(const int cell1, const int cell2) const;
    const uint Root(uint cell_index) const;
    const bool Set_Find(uint cell1, uint cell2) const;
    void Set_Unite(uint cell1, uint cell2);
public:
    Maze(uint num_rows, uint num_columns);
    Maze(std::string &mazelist);
    ~Maze();
    void Generate();
    const bool Solve();
    void Print();
    void Visualize();
};

#endif /* end of include guard: MAZEUTILS_H_URRNJKT0 */

