#include <iostream>
#include "mazeutils.h"

using namespace std;

int main(int argc, char const *argv[])
{
    // seed rand
    srand(time(NULL));

    // parse the arguments
    if (argc != 3)
        cout << "Usage: makemaze <columns> <rows>" << endl;
    uint num_rows = atoi(argv[1]);
    uint num_columns = atoi(argv[2]);

    // instantiate a maze, print it
    Maze maze(num_rows, num_columns);
    maze.Print();
    return 0;
}

