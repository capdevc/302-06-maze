CC = g++
CFLAGS = -g -O2 -Wall -std=c++0x

all: mazemake mazesolve

mazemake: mazemake.o mazeutils.o
	$(CC) $(CFLAGS) -o $@ $^

mazesolve: mazesolve.o mazeutils.o
	$(CC) $(CFLAGS) -o $@ $^

.cpp.o: 
	$(CC) $(CFLAGS) -c $<

clean:
	@rm -f *.o mazemake mazesolve
