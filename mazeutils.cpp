#include <iostream>
#include <sstream>
#include <stack>
#include "mazeutils.h"

using namespace std;

// Maze constructor
Maze::Maze(uint num_rows, uint num_columns)
    : num_rows_(num_rows), num_columns_(num_columns), size_(num_columns *num_rows)
{
    // allocate memory for a cells * walls 2d array, and set arrays
    adjacents_ = new int*[size_];
    adjacents_[0] = new int[WALLS * size_];
    set_map_ = new uint[size_];
    set_size_ = new uint[size_];
    set_count_ = size_;

    // set our cell pointers to "WALLS" chunks apart
    for (uint i = 1; i < size_; ++i)
        adjacents_[i] = adjacents_[i-1] + WALLS;
    // set the adjacency list to -1 for a wall, -2 for the boundary
    // and initialize the set map in the same loop
    for (uint i = 0; i < size_; ++i) {
        for (uint j = 0; j < WALLS; ++j) {
            if (Border(i, j)) adjacents_[i][j] = -2;
            else adjacents_[i][j] = -1;
        }
        set_map_[i] = i;
        set_size_[i] = 1;
    }
    Generate();
}

// Maze ctor that reads in a pregenerated maze
Maze::Maze(string &mazelist)
{
    stringstream mazebuffer(mazelist);
    string header;
    int c1, c2;

    mazebuffer >> header >> num_rows_ >> num_columns_;
    size_ = num_rows_ * num_columns_;

    // allocate memory for a cells * walls 2d array, and set arrays
    adjacents_ = new int*[size_];
    adjacents_[0] = new int[WALLS * size_];
    set_map_ = new uint[size_];
    set_size_ = new uint[size_];
    set_count_ = 1;

    // set our cell pointers to "WALLS" chunks apart
    for (uint i = 1; i < size_; ++i)
        adjacents_[i] = adjacents_[i-1] + WALLS;

    // set all appropriate walls as open by filling the
    // adjacency list
    for (uint i = 0; i < size_; ++i) {
        for (uint j = 0; j < WALLS; ++j) {
            if (Border(i,j)) adjacents_[i][j] = -2;
            else adjacents_[i][j] = Neighbor(i, j);
        }
        // set the set data just in case
        set_map_[i] = 0;
        set_size_[i] = size_;
    }

    // insert walls between cells as required by the maze
    while (mazebuffer >> c1 >> c2) {
        adjacents_[c1][Wall(c1, c2)] = -1;
        adjacents_[c2][Wall(c2, c1)] = -1;
    }
}

// Maze dtor
Maze::~Maze()
{
    // free the memory we allocated at the beginning
    delete[] adjacents_[0];
    delete[] adjacents_;
    delete[] set_map_;
    delete[] set_size_;
}

// check to see if this cell/wall combination is off the maze
const bool Maze::Border(const int cell, const uint wall) const
{
    if ((wall == NORTH) && (cell < (int) num_columns_)) return true;
    else if ((wall == EAST) && !((cell + 1) % num_columns_)) return true;
    else if ((wall == SOUTH) && (cell >= (int) (size_ - num_columns_))) return true;
    else if ((wall == WEST) && !(cell % num_columns_)) return true;
    else return false;
}

// return the cell neighboring the input cell to wall direction
const int Maze::Neighbor(const int cell, const uint wall) const
{

    if (wall == NORTH) return (cell - num_columns_);
    else if (wall == EAST) return (cell + 1);
    else if (wall == SOUTH) return (cell + num_columns_);
    else if (wall == WEST) return (cell - 1);
    else return -1;
}

// return which wall is between two cells
const uint Maze::Wall(const int cell1, const int cell2) const
{
    if (cell2 > cell1) {
        if ((cell2 - cell1) == 1) return EAST;
        if ((cell2 - cell1) == num_columns_) return SOUTH;
    } else if (cell1 > cell2) {
        if ((cell1 - cell2) == 1) return WEST;
        if ((cell1 - cell2) == num_columns_) return NORTH;
    }
    return 5;
}

// iterate through the set map until we find the root
const uint Maze::Root(uint cell_index) const
{
    while (cell_index != set_map_[cell_index]) {
        // next line compacts the set map one level each time we go through
        set_map_[cell_index] = set_map_[set_map_[cell_index]];
        cell_index = set_map_[cell_index];
    }
    return cell_index;
}

// check if two cells are part of the same set
const bool Maze::Set_Find(uint cell1, uint cell2) const
{
    return (Root(cell1) == Root(cell2));
}

// compare the root nodes, merge the smaller set into the bigger one
void Maze::Set_Unite(uint cell1, uint cell2)
{
    uint root1 = Root(cell1);
    uint root2 = Root(cell2);
    if (set_size_[root1] >= set_size_[root2]) {
        set_map_[root2] = root1;
        set_size_[root1] += set_size_[root2];
    } else {
        set_map_[root1] = root2;
        set_size_[root2] += set_size_[root1];
    }
    --set_count_; // decerement the set counter so we can see when we're at 1
}

// generate a maze
void Maze::Generate()
{
    // iterate until we're down to 1 set
    while (set_count_ > 1) {
        // pick a random cell and a random direction
        uint cell = rand() % size_;
        uint wall = rand() % WALLS;
        uint neighbor;
        // if there's a wall there that's not a border, and the two cells on
        // either side aren't in the same set, knock it down, unite the sets
        // and link the cells in the adjacency list
        if ((adjacents_[cell][wall] == -1) && !Border(cell, wall)) {
            neighbor = Neighbor(cell, wall);
            if (!Set_Find(cell, neighbor)) {
                Set_Unite(cell, neighbor);
                adjacents_[cell][wall] = neighbor;
                adjacents_[neighbor][(wall + WALLS/2) % WALLS] = cell;
            }
        }
    }
}

const bool Maze::Solve()
{
    int parent[size_]; // track parents for traceback
    int target = size_ - 1; // our target cell
    int cell = 0; // our starting cell
    stack<int> cells; // stack of cells to check

    for (uint i = 0; i < size_; ++i)
        parent[i] = i;
    parent[cell] = -1;

    // start with our initial cell
    cells.push(cell);
    while (!cells.empty()) {
        // grab the top of stack
        cell = cells.top();
        cells.pop();
        // quit the loop if it's the target
        if (cell == target) break;
        // otherwise, add its unseen adjacents to the stack
        for (uint i = 0; i < WALLS; ++i) {
            int adjacent = adjacents_[cell][i];
            if ((adjacent >= 0) && (parent[adjacent] == adjacent)) {
                cells.push(adjacent);
                parent[adjacent] = cell;
            }
        }
    }

    // step backwards through the parent list and print
    if (cell == target) {
        // empty our stack for reuse
        while (!cells.empty()) cells.pop();
        cout << "PATH " << num_rows_ << " " << num_columns_ << endl;
        // push cells onto the stack
        while (cell != -1) {
            cells.push(cell);
            cell = parent[cell];
        }
        // print the cells
        while (!cells.empty()) {
            cout << cells.top() << endl;
            cells.pop();
        }
        return true;
    } else return false;

}

// print the maze walls to std out
void Maze::Print()
{
    cout << "MAZE " << num_rows_ << " " << num_columns_ << endl;
    for (uint i = 0; i < size_; ++i)
        for (uint j = 0; j < WALLS/2; ++j)
            if (adjacents_[i][j] == -1)
                cout << i << " " << Neighbor(i, j) << endl;
}
