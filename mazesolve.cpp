#include <iostream>
#include <sstream>
#include "mazeutils.h"

using namespace std;

int main(int argc, char const *argv[])
{
    // read stdin into stringstream
    stringstream buffer(stringstream::in | stringstream::out);
    string line;
    while (getline(cin, line)) buffer << line << " ";

    // convert that stringstream to a string and pass it 
    // to the maze constructor
    string mazelist = buffer.str();
    Maze m(mazelist);

    // solve the maze
    m.Solve();

    return 0;
}
